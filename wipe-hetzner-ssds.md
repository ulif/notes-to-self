# Wipe SSDs on Hetzner systems

1) Make sure, you really want to wipe everything

2) Reboot to rescue

3) Stop and disable any existing raid arrays:

    # cat /proc/mdstat
    # mdadm --stop /dev/mdX
    # mdadm --zero-superblock /dev/sdXY

   for all mds and all partitions (not: disks) /dev/sdXY

   Check status:

    # hdparm -I /dev/sda
    ...
    Security:
        Master password revision code = 65534
                supported
        not     enabled
        not     locked
        not     frozen
        not     expired: security count
                supported: enhanced erase
        2min for SECURITY ERASE UNIT. 2min for ENHANCED SECURITY ERASE UNIT.
    ...

   If `frozen` is set (i.e.: no `not` in front of it), you have to unfreeze the device.

3a) Unfreeze

   Do this only, if the device is freezed (see above):

    # apt install pm-utils
    # pm-suspend

   This will make the server sleep. Wake it up with the WakeOnLan (WOL)
   functionality in the Hetzner web interface.

   Wait about 30 secs.


4) Wipe

   # hdparm --user-master u --security-set-pass p /dev/sdX   # sets password 'p'
   # time hdparm --user-master u --security-erase-enhanced p /dev/sdX
   # hdparm --user-master u --security-set-pass p /dev/sdX
   # time hdparm --user-master u --security-erase p /dev/sdX

  Afterwards a

   # hdparm -I /dev/sdX

  should give: not enabled, not locked, not frozen.

  Other check:

   # dd if=/dev/sdX bs=1M count=5 | hd

  should show, that there are only zeros in the given 5 MB data.


## References:

https://wiki.ubuntuusers.de/SSD/Secure-Erase/
https://grok.lsu.edu/Article.aspx?articleid=16716
https://superuser.com/questions/1213715/hdparm-error-sg-io-bad-missing-sense-data

